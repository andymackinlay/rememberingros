const googleapi = require(`./src/googleapis`);
const path = require(`path`);
const mkdirp = require(`mkdirp`);
const fs = require(`fs`);

const log = str => console.log(`🚗 `, str);
const FOLDER = `application/vnd.google-apps.folder`;
const GOOGLE_APPS_RE = /^application\/vnd\.google-apps\./;


exports.onPreInit = () => {
  console.log("Loading forked google drive plugin");
}

exports.onPreBootstrap = (
  { graphql, actions },
  { folderId, keyFile, key, destination }
) => {
  return new Promise(async resolve => {
    log(`Started downloading content from folder ${folderId} with key ${key.client_email}`);

    // Get token and fetch root folder.
    const token = keyFile ? 
      await googleapi.getToken({ keyFile }) :
      await googleapi.getToken({ key });
    const cmsFiles = await googleapi.getFolder(folderId, token);

    // Create content directory if it doesn't exist.
    mkdirp(destination);

    // Start downloading recursively through all folders.
    console.time(`Downloading content`);
    recursiveFolders(cmsFiles, undefined, token, destination).then(() => {
      console.timeEnd(`Downloading content`);
      resolve();
    });
  });
}

function includable(file) {
  return !file.mimeType.match(GOOGLE_APPS_RE) || file.mimeType === FOLDER;
}

function recursiveFolders(array, parent = '', token, destination) {
  return new Promise(async (resolve, reject) => {
    let promises = [];
    let filesToDownload = array.filter(file => includable(file));

    for (let file of filesToDownload) {
      log(`handling file ${file.name} in ${parent}`);
      // Check if it`s a folder or a file
      if (file.mimeType === FOLDER) {
        // If it`s a folder, create it in filesystem
        log(`Creating folder ${parent}/${file.name}`);
        mkdirp(path.join(destination, parent, file.name));

        // Then, get the files inside and run the function again.
        const files = await googleapi.getFolder(file.id, token);
        promises.push(
          recursiveFolders(files, `${parent}/${file.name}`, token, destination)
        );
      } else {
        promises.push(
          new Promise(async (resolve, reject) => {
            log(`storing file ${file.name}`);

            // If it`s a file, download it and convert to buffer.
            const dest = path.join(destination, parent, file.name);

            if (fs.existsSync(dest)) {
              resolve(file.name);
              return log(`Using cached ${file.name}`);
            }

            const buffer = await googleapi.getFile(file.id, token);

            // Finally, write buffer to file.
            fs.writeFile(dest, buffer, err => {
              if (err) return log(err);

              log(`Saved file ${file.name}`);
              resolve(file.name);
            });
          })
        );
      }
    }

    Promise.all(promises).then(() => resolve());
  });
}

const fileExtensionsByMime = new Map([
  ['text/html', '.html'],
  ['application/zip', '.zip'],
  ['text/plain', '.txt'],
  ['application/rtf', '.rtf'],
  ['application/vnd.oasis.opendocument.text', '.odt'],
  ['application/pdf', '.pdf'],
  ['application/vnd.openxmlformats-officedocument.wordprocessingml.document', '.docx'],
  ['application/epub+zip', '.epub']
]);
