const { GoogleToken } = require('gtoken');
const request = require('request');

const getToken = ({ keyFile, key }) => {
  return new Promise((resolve, reject) => {
    const scope = ['https://www.googleapis.com/auth/drive']
    const gtoken = keyFile ?
      new GoogleToken({
        keyFile,
        scope: scope
      }) :
      new GoogleToken({
        email: key.client_email,
        scope: scope,
        key: key.private_key.replace(/(\\r)|(\\n)/g, '\n')
      });

    gtoken.getToken((err, token) => {
      if (err) {
        reject(err);
      } else {
        resolve(token);
      }
    });
  });
};

const getFolder = (folderId, token) => {
  return new Promise((resolve, reject) => {
    request(
      {
        uri: `https://www.googleapis.com/drive/v3/files`,
        auth: {
          bearer: token,
        },
        qs: {
          q: `'${folderId}' in parents`,
          pageSize: 1000,
        },
      },
      (err, res, body) => {
        if (err) {
          reject(err);
        } else {
          const content = JSON.parse(body);
          if (content['error']) {
            reject(content['error']); 
          } else {
            const files = content.files;
            console.log(`Found ${files ? files.length : 'NA'} files`)
            resolve(files);
          }
        }
      }
    );
  });
};

const getFile = (fileId, token) => {
  return new Promise((resolve, reject) => {
    requestFile(resolve, reject, fileId, token, 1100);
  });
};

const getGDoc = (fileId, token, mimeType) => {
  return new Promise((resolve, reject) => {
    request({
      uri: `https://www.googleapis.com/drive/v3/files/${fileId}/export`,
      auth: {
        bearer: token
      },
      encoding: null,
      qs: {
        mimeType: mimeType
      }
    }, (err, res, body) => {
      if (err) {
        reject(err)
      } else {
        resolve(body)
      }
    })
  })
}

module.exports = {
  getToken,
  getFolder,
  getFile,
  getGDoc
};

function requestFile(resolve, reject, fileId, token, delay) {
  request(
  {
    uri: `https://www.googleapis.com/drive/v3/files/${fileId}`,
    auth: {
      bearer: token,
    },
    encoding: null,
    qs: {
      alt: 'media',
    },
  },
  (err, res, body) => {
    if (err) {
      reject(err);
    } else if (res.statusCode == 403) {
      setTimeout(() => {
        requestFile(resolve, reject, fileId, token, delay * 2);
      }, delay * 2);
    } else {
      resolve(body);
    }
  });
};
