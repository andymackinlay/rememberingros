
require('dotenv').config()

module.exports = {
  siteMetadata: {
    title: `Remembering Ros`,
    description: `Memories of a delightful lady`,
    author: `Andy`,
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-drive',
      options: {
        folderId: process.env.GOOGLE_DRIVE_FOLDER_ID,
        key: {
          private_key: (process.env.GOOGLE_PRIVATE_KEY || ""
            ).replace("\\n", "\n"),
          client_email: process.env.GOOGLE_CLIENT_EMAIL,
        },
        destination: `${__dirname}/content`,
        exportGDocs: false,
      }
    },
    {
      resolve: `gatsby-plugin-typescript`,
      options: {
        isTSX: true, // defaults to false
        //jsxPragma: `jsx`, // defaults to "React" ??
        allExtensions: true
      },
    },
    {
      resolve: `gatsby-plugin-styled-jsx`,
      options: {
        jsxPlugins: ["styled-jsx-plugin-postcss"],
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/content/images`,
      },
    },
    {
      resolve: `gatsby-plugin-favicon`,
      options: {
        logo: `${__dirname}/static/favicon.png`,
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#372428`,
        theme_color: `#372428`,
        display: `minimal-ui`,
        icon: `static/favicon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/content`,
      },
    },
    `gatsby-transformer-csv`,
  ],
}
